<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Datautama | Product</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.3.0/css/all.min.css"
          integrity="sha512-SzlrxWUlpfuzQ+pcUCosxcglQRNAq/DZjVsC0lE40xsADsfeQoEypE+enwcOiGjk/bSuGGKHEyjSoQ1zVisanQ=="
          crossorigin="anonymous" referrerpolicy="no-referrer"/>
</head>
<body>

<header>
    <!-- Fixed navbar -->
    <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
        <div class="container-fluid">
            <a class="navbar-brand" href="#">Data Utama</a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarCollapse"
                    aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarCollapse">
                <ul class="navbar-nav me-auto mb-2 mb-md-0">
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="{{route('web.product.index')}}">Product</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('web.transaction.index')}}">Transaction</a>
                    </li>
                </ul>
                <form class="d-flex" role="search">
                    <button class="btn btn-outline-warning">Logout</button>
                </form>
            </div>
        </div>
    </nav>
</header>
<br/><br/>

<main class="container mt-5">
    <table class="table table-bordered mb-5">
        <thead>
        <tr>
            <th colspan="5">
                <form action="{{route('web.product.index')}}" method="get">
                    <input type="text" name="search" value="{{$s}}" class="form-control"
                           placeholder="Search Name Product">
                </form>
            </th>
            <th colspan="2">
                <div class="btn btn-dark w-100" onclick="addProduct()"><i class="fa-solid fa-plus"></i></div>
            </th>
        </tr>
        <tr class="table-success">
            <th>#</th>
            <th>Name</th>
            <th width="160">Price</th>
            <th>Stock</th>
            <th>Description</th>
            <th colspan="2">Act</th>
        </tr>
        </thead>
        <tbody>
        @foreach($datas as $data)
            <tr>
                <th scope="row">{{ $data->id }}</th>
                <td>{{ $data->name }}</td>
                <td>@currency($data->price)</td>
                <td>{{ $data->stock }}</td>
                <td>{{ $data->description }}</td>
                <td>
                    <div class="btn btn-sm btn-primary w-100" onclick="editProduct('{{$data->id}}')"><i class="fa-solid fa-pen-to-square"></i></div>
                </td>
                <td>
                    <div class="btn btn-sm btn-warning w-100" onclick="deleteProduct('{{$data->id}}')"><i class="fa-solid fa-delete-left"></i></div>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    {{-- Pagination --}}
    <div class="d-flex justify-content-center">
        {!! $datas->links() !!}
    </div>
</main>

<div class="modal" id="productModal" tabindex="-1">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Modal title</h5>
                <button type="button" class="btn-close" onclick="closeProduct()" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form id="formProduct">
                    <input type="hidden" name="id" id="id">
                    @csrf
                    <div class="mb-3">
                        <label class="form-label">Product Name</label>
                        <input type="text" name="name" id="name" placeholder="Product Name" class="form-control" required>
                    </div>
                    <div class="mb-3">
                        <label class="form-label">Product Price</label>
                        <input type="number" name="price" id="price" placeholder="Product Price" class="form-control" required>
                    </div>
                    <div class="mb-3">
                        <label class="form-label">Product Stock</label>
                        <input type="number" name="stock" id="stock" placeholder="Product Stock" class="form-control" required>
                    </div>
                    <div class="mb-3">
                        <label class="form-label">Product Description</label>
                        <textarea name="description" id="description" class="form-control" required placeholder="Product Description"></textarea>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" onclick="closeProduct()">Close</button>
                <button type="button" class="btn btn-primary" id="btnAct" onclick="submitProduct()">Save changes</button>
            </div>
        </div>
    </div>
</div>

<script src="https://code.jquery.com/jquery-3.6.4.min.js" integrity="sha256-oP6HI9z1XaZNBrJURtCoUT5SUnxFr8s3BzRl+cbzUq8=" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.min.js"
        integrity="sha384-mQ93GR66B00ZXjt0YO5KlohRA5SY2XofN4zfuZxLkoj1gXtW8ANNCe9d5Y3eG5eD"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.6/dist/umd/popper.min.js"
        integrity="sha384-oBqDVmMz9ATKxIep9tiCxS/Z9fNfEXiDAYTujMAeBAsjFuCZSmKbSSUnQlmh/jp3"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.3.0/js/all.min.js"
        integrity="sha512-2bMhOkE/ACz21dJT8zBOMgMecNxx0d37NND803ExktKiKdSzdwn+L7i9fdccw/3V06gM/DBWKbYmQvKMdAA9Nw=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script>
    function addProduct(){
        $(".modal-title").html('Form Add Product!');
        $("#btnAct").html('Tambah!')
        $("#productModal").modal('show');
    }

    function closeProduct(){
        $(".modal-title").html('');
        $("#id").val("");
        $("#name").val("");
        $("#price").val("");
        $("#stock").val("");
        $("#description").val("");
        $("#productModal").modal('hide');
    }

    function editProduct(id){
        $.get('/web/product/show/'+id, function (resp){
            if(resp.message === 'SUCCESS'){
                $(".modal-title").html('Form Update Product!');
                $("#btnAct").html('Ubah!')
                $("#id").val(resp.data.id);
                $("#name").val(resp.data.name);
                $("#price").val(resp.data.price);
                $("#stock").val(resp.data.stock);
                $("#description").val(resp.data.description);
                $("#productModal").modal('show');
            }else{
                alert('Maaf telah terjadi kesalahan pada sistem');
            }
        })
    }

    function deleteProduct(id){
        if(confirm("Apakah anda yakin untuk hapus data ini?")===true){
            $.ajax({
                url: '{{route('web.product.delete')}}',
                type: 'POST',
                data: {id: id, "_token": "{{ csrf_token() }}"},
                success: function (resp){
                    if(resp.message === 'DELETED'){
                        location.reload();
                    }else{
                        alert('Maaf telah terjadi kesalahan sistem');
                        closeProduct();
                    }
                }
            })
        }
    }

    function submitProduct(){
        id = $("#id").val();
        if(id === "" || id === null){
            $.ajax({
                url: '{{route('web.product.store')}}',
                type: 'POST',
                data: $("#formProduct").serialize(),
                success: function (resp){
                    if(resp.message === 'INSERTED'){
                        location.reload();
                    }else{
                        alert('Maaf telah terjadi kesalahan sistem');
                        closeProduct();
                    }
                }
            })
        }else{
            $.ajax({
                url: '/web/product/update/'+id,
                type: 'POST',
                data: $("#formProduct").serialize(),
                success: function (resp){
                    if(resp.message === 'UPDATED'){
                        location.reload();
                    }else{
                        alert('Maaf telah terjadi kesalahan sistem');
                        closeProduct();
                    }
                }
            })
        }
    }
</script>
</body>
</html>
