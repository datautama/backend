FROM php:8.1-fpm-alpine

RUN apk add --no-cache ${PHPIZE_DEPS} libpq-dev oniguruma-dev shadow icu-dev zlib-dev libpng-dev
RUN apk del --no-cache ${PHPIZE_DEPS}
RUN docker-php-ext-install pdo pdo_pgsql pgsql sockets bcmath mbstring intl gd
RUN curl -sS https://getcomposer.org/installer​ | php -- \
     --install-dir=/usr/local/bin --filename=composer

COPY --from=composer:latest /usr/bin/composer /usr/bin/composer

WORKDIR /app
COPY . .
RUN composer install --no-interaction
RUN php artisan key:generate
RUN php artisan jwt:secret

CMD php artisan serve --host=0.0.0.0 --port=8080
