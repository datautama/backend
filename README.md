# Laravel + PostgrSQL + Docker
## For Assignment Employee PT. Data Andalan Utama

### Requirement
1. PHP 8.1
2. Additional Extension PHP for Laravel

### Local Install
```sh
git clone https://gitlab.com/datautama/backend.git && cd backend
composer install
cp -r .env.example .env
php artisan key:generate
php artisan jwt:secret
php artisan migrate --seed
php artisan serve
```

### Valet Install
```sh
git clone https://gitlab.com/datautama/backend.git && cd backend
valet isolate php@8.1
valet composer install
cp -r .env.example .env
valet php artisan key:generate
valet php artisan jwt:secret
valet php artisan migrate --seed
```
open browser, _http://backend.test_

### Docker-composer Install
```sh
git clone https://gitlab.com/datautama/backend.git && cd backend
cp -r .env.example .env
docker-compose up
```
open browser, _http://localhost:3333_ for main app. and _http://localhost:8282_ for adminer database

### Running Test Using Postman
Import _Postman Collection json_ to postman app, and run

# Thanks
