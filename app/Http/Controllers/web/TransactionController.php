<?php

namespace App\Http\Controllers\web;

use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\Transaction;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class TransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = Transaction::query()->with('product')
            ->when($request->search, function ($q) use ($request) {
                return $q->whereRaw('LOWER(reference_no) like ?', '%'.Str::lower($request->search).'%');
            })
            ->paginate(5);
        $products = Product::all();
        return view('transaction', ['datas'=>$data, 's'=>$request->search, 'products'=>$products]);
    }

    public function store(Request $request)
    {
        $rules = [
            'price' => 'required|numeric',
            'quantity' => 'required|numeric',
            'payment_amount' => 'required:numeric',
            'product_id'=>'required|numeric|exists:products,id'
        ];

        $validator = Validator::make($request->all(), $rules, []);

        if ($validator->fails()) {
            return response()->json([
                'code' => Response::HTTP_UNPROCESSABLE_ENTITY,
                'message' => $validator->errors()->all()
            ]);
        }

        DB::beginTransaction();
        try{
            Transaction::create($validator->validated());

            DB::commit();

            return response()->json([
                'code' => Response::HTTP_CREATED,
                'message' => 'INSERTED',
            ], Response::HTTP_CREATED);
        }catch (\Exception $e){
            DB::rollback();

            return response()->json([
                'code' => Response::HTTP_INTERNAL_SERVER_ERROR,
                'message' => $e->getMessage(),
            ]);

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Transaction::with('product')->find($id);
        return response()->json(['code'=>Response::HTTP_OK, 'message'=>'SUCCESS', 'data'=>$data]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'price' => 'required|numeric',
            'quantity' => 'required|numeric',
            'payment_amount' => 'required:numeric',
            'product_id'=>'required|numeric|exists:products,id'
        ];

        $validator = Validator::make($request->all(), $rules, []);

        if ($validator->fails()) {
            return response()->json([
                'code' => Response::HTTP_UNPROCESSABLE_ENTITY,
                'message' => $validator->errors()->all()
            ]);
        }

        DB::beginTransaction();
        try{
            Transaction::query()->findOrFail($id)->update($validator->validated());

            DB::commit();

            return response()->json([
                'code' => Response::HTTP_ACCEPTED,
                'message' => 'UPDATED',
            ], Response::HTTP_ACCEPTED);
        }catch (\Exception $e){
            DB::rollback();

            return response()->json([
                'code' => Response::HTTP_INTERNAL_SERVER_ERROR,
                'message' => $e->getMessage(),
            ]);

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $id = $request->id;

        DB::beginTransaction();
        try{
            Transaction::query()->findOrFail($id)->delete();

            DB::commit();

            return response()->json([
                'code' => Response::HTTP_ACCEPTED,
                'message' => 'DELETED',
            ], Response::HTTP_ACCEPTED);
        }catch (\Exception $e){
            DB::rollback();

            return response()->json([
                'code' => Response::HTTP_INTERNAL_SERVER_ERROR,
                'message' => $e->getMessage(),
            ]);

        }
    }
}
