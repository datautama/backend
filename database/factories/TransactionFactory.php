<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Transaction>
 */
class TransactionFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'reference_no'=>$this->faker->bothify('INV/??-##'),
            'price'=>$this->faker->numberBetween('1000000', '3000000'),
            'quantity'=>$this->faker->numberBetween('20', '60'),
            'payment_amount'=>$this->faker->numberBetween('2000000', '6000000')
        ];
    }
}
